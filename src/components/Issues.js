import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { fetchIssues } from '../actions/issuesActions';
import { connect } from 'react-redux';

class Issues extends Component {
  componentDidMount() {
    this.props.fetchIssues(this.props.match.params.name);
  }
  render() {
    const { issues, loadingIssues } = this.props;
    return (
      <div className="card">
        <div className="card-header">{this.props.match.params.name}/Issues</div>
        <ul className="list-group list-group-flush">
          {loadingIssues ? (
            issues.data.map((item) => (
              <li className="list-group-item" key={item.id}>
                #{item.number}{' '}
                <Link
                  to={`/${this.props.match.params.name}/issues/${item.number}`}
                >
                  {item.title}
                </Link>{' '}
                <p>{item.comments} Comments</p>
              </li>
            ))
          ) : (
            <p>Loading...</p>
          )}
        </ul>
      </div>
    );
  }
}

const mapStateToprops = (state) => ({
  issues: state.issues.issues,
  loadingIssues: state.issues.loading,
});
const mapDispatchToProps = (dispatch) => {
  return {
    fetchIssues: (reposUrl) => dispatch(fetchIssues(reposUrl)),
  };
};

export default connect(mapStateToprops, mapDispatchToProps)(Issues);
