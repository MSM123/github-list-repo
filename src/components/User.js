import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUsers } from '../actions/usersActions';
import { fetchRepos } from '../actions/reposActions';
import { Link } from 'react-router-dom';

class User extends Component {
  
  componentDidMount() {
    this.props.fetchUsers();
    this.props.fetchRepos();
  }

  render() {
    const { users, repos, loadingUsers, loadingRepos } = this.props;
    return (
      <div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            marginTop: '10px',
            marginBottom: '10px',
          }}
        >
          {loadingUsers ? (
            <div>
              <img
                src={users.data.avatar_url}
                alt="avatar"
                title="avatar"
                height="100px"
              />
              <div style={{ marginLeft: '10px' }}>
                <h1>{users.data.login}</h1>
                {users.data.location ? <h6>{users.data.location}</h6> : null}
              </div>
            </div>
          ) : (
            <p>Loading...</p>
          )}
        </div>
        <div className="card">
          <div className="card-header">Repositories</div>
          {loadingRepos ? (
            repos.data.map((item) => (
              <Link to={`/${item.name}/issues`} key={item.id}>
                <p>{item.name}</p>
              </Link>
            ))
          ) : (
            <p>Loading...</p>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToprops = (state) => ({
  users: state.users.users,
  loadingUsers: state.users.loading,
  repos: state.repos.repos,
  loadingRepos: state.repos.loading,
});
const mapDispatchToProps = (dispatch) => {
  return {
    fetchUsers: () => dispatch(fetchUsers()),
    fetchRepos: () => dispatch(fetchRepos()),
  };
};

export default connect(mapStateToprops, mapDispatchToProps)(User);
