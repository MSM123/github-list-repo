import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { fetchIssuesDetails } from '../actions/issuesDetailsActions';
import { fetchComments} from '../actions/commentsAction';

class IssuesDetails extends Component {
 
  componentDidMount() {
    this.props.fetchIssuesDetails(
      this.props.match.params.name,
      this.props.match.params.number
    );
    this.props.fetchComments(
      this.props.match.params.name,
      this.props.match.params.number
    );
  }

  render() {
    const {
      issuesDetails,
      loadingIssuesDetails,
      comments,
      loadingComments,
    } = this.props;
    return (
      <div className="card ">
        <div className="card-header">
          {this.props.match.params.name}/issues/#
          {this.props.match.params.number}
        </div>
        {loadingIssuesDetails ? (
          <div className="card-body text-white bg-secondary mb-3">
            <p className="card-text">{issuesDetails.data.body}</p>
          </div>
        ) : (
          <p>Loading...</p>
        )}
        {loadingComments ? (
          <ul className="list-group list-group-flush">
            {comments.data.map((item) => (
              <li className="list-group-item" key={item.id}>
                {item.body}
              </li>
            ))}
          </ul>
        ) : (
          <p>Loading...</p>
        )}
      </div>
    );
  }
}

const mapStateToprops = (state) => ({
  issuesDetails: state.issuesDetails.issuesDetails,
  loadingIssuesDetails: state.issuesDetails.loading,
  comments: state.comments.comments,
  loadingComments: state.comments.loading,
});
const mapDispatchToProps = (dispatch) => {
  return {
    fetchIssuesDetails: (issuesName, issuesNum) =>
      dispatch(fetchIssuesDetails(issuesName, issuesNum)),
    fetchComments: (issuesName, issuesNum) =>
      dispatch(fetchComments(issuesName, issuesNum)),
  };
};

export default connect(mapStateToprops, mapDispatchToProps)(IssuesDetails);
