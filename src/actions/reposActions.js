import { FETCH_REPOS } from './types';
import axios from 'axios';

const reposAct = (repos) => {
  return {
    type: FETCH_REPOS,
    payload: repos,
    loading: true,
  };
};

export const fetchRepos = () => (dispatch) => {
  axios
    .get(`https://api.github.com/users/facebook/repos`, {
      headers: {
        Authorization: `token 3fd996617d70f6ca9647736789009455b11230c4`,
      },
    })
    .then((repos) => dispatch(reposAct(repos)));
};
