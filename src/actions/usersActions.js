import { FETCH_USERS } from './types';
import axios from 'axios';

const userAct = (users) => {
  return {
    type: FETCH_USERS,
    payload: users,
    loading: true,
  };
};

export const fetchUsers = () => (dispatch) => {
  axios
    .get(`https://api.github.com/users/facebook`, {
      headers: {
        Authorization: `token 3fd996617d70f6ca9647736789009455b11230c4`,
      },
    })
    .then((users) => dispatch(userAct(users)));
};
