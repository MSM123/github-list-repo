import { FETCH_ISSUES_DETAILS } from './types';
import axios from 'axios';

const issuesDetailsAct = (issuesDetails) => {
  return {
    type: FETCH_ISSUES_DETAILS,
    payload: issuesDetails,
    loading: true,
  };
};

export const fetchIssuesDetails = (issuesName,issuesNum) => (dispatch) => {
  axios
    .get(`https://api.github.com/repos/facebook/${issuesName}/issues/${issuesNum}`, {
      headers: {
        Authorization: `token 3fd996617d70f6ca9647736789009455b11230c4`,
      },
    })
    .then((issuesDetails) => dispatch(issuesDetailsAct(issuesDetails)));
};
