import { FETCH_COMMENTS , NEW_COMMENTS} from './types';
import axios from 'axios';

const commentsAct = (comments) => {
  return {
    type: FETCH_COMMENTS,
    payload: comments,
    loading: true,
  };
};

export const fetchComments = (issuesName,issuesNum) => (dispatch) => {
  axios
    .get(`https://api.github.com/repos/facebook/${issuesName}/issues/${issuesNum}/comments`, {
      headers: {
        Authorization: `token 3fd996617d70f6ca9647736789009455b11230c4`,
      },
    })
    .then((comments) => dispatch(commentsAct(comments)));
};

