import { FETCH_ISSUES } from './types';
import axios from 'axios';

const issuesAct = (issues) => {
  return {
    type: FETCH_ISSUES,
    payload: issues,
    loading: true,
  };
};

export const fetchIssues = (reposUrl) => (dispatch) => {
  axios
    .get(`https://api.github.com/repos/facebook/${reposUrl}/issues`, {
      headers: {
        Authorization: `token 3fd996617d70f6ca9647736789009455b11230c4`,
      },
    })
    .then((issues) => dispatch(issuesAct(issues)));
};
