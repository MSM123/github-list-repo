import { FETCH_ISSUES } from '../actions/types';

const initialState = {
  issues: [],
  loading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_ISSUES:
      return {
        ...state,
        issues: action.payload,
        loading: action.loading,
      };
    default:
      return state;
  }
}
