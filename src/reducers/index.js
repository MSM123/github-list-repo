import { combineReducers } from 'redux';
import usersReducer from './usersReducer';
import reposReducer from './reposReducer';
import issuesReducer from './issuesReducer';
import issuesDetailsReducer from './issuesDetailsReducer';
import commentsReducer from './commentsReducer';

export default combineReducers({
  users: usersReducer,
  repos: reposReducer,
  issues: issuesReducer,
  issuesDetails: issuesDetailsReducer,
  comments: commentsReducer
});
