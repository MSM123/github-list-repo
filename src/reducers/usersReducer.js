import { FETCH_USERS } from '../actions/types';

const initialState = {
  users: [],
  loading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_USERS:
      return {
        ...state,
        users: action.payload,
        loading: action.loading,
      };
    default:
      return state;
  }
}
