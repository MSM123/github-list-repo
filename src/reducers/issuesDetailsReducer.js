import { FETCH_ISSUES_DETAILS } from '../actions/types';

const initialState = {
  issuesDetails: [],
  loading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_ISSUES_DETAILS:
      return {
        ...state,
        issuesDetails: action.payload,
        loading: action.loading,
      };
    default:
      return state;
  }
}
