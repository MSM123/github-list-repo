import { FETCH_COMMENTS} from '../actions/types';

const initialState = {
  comments: [],
  loading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_COMMENTS:
      return {
        ...state,
        comments: action.payload,
        loading: action.loading,
      };
    default:
      return state;
  }
}
