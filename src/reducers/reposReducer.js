import { FETCH_REPOS } from '../actions/types';

const initialState = {
  repos: [],
  loading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_REPOS:
      return {
        ...state,
        repos: action.payload,
        loading: action.loading,
      };
    default:
      return state;
  }
}
