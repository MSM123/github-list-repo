import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import User from './components/User';
import Issues from './components/Issues';
import IssuesDetails from './components/IssuesDetails';
import Dashboard from './components/Dashboard';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Dashboard />
        <Switch>
          <Route exact path="/" component={User} />
          <Route exact path="/:name/issues" component={Issues} />
          <Route exact path="/:name/issues/:number" component={IssuesDetails} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
